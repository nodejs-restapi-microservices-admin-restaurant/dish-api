# Documentation
* [Dish API](#dish-api)

## Dish API
### GET /dishes
Returns all dish instance.

Example response:
```json
[
    {
        "ingredients": [
            {
                "price": 10,
                "_id": "5dd09c2a0cdb777077a0bec8",
                "title": "Творог",
                "restInStock": 10000,
                "description": "",
                "__v": 0
            }
        ],
        "additionalIngredients": [
            {
                "price": 40,
                "_id": "5dd1b0a91c9d440000c0d815",
                "title": "Шоколад",
                "restInStock": 450,
                "description": "Черный шоколад с высоким содержанием какао-масла",
                "__v": 0
            }
        ],
        "price": 80,
        "_id": "5dd0a0bd40d38b71a3adc7de",
        "title": "Сырники из творога",
        "description": "Bкусно",
        "category": {
            "_id": "5dd19ed91c9d440000c0d802",
            "title": "Десерты",
            "description": "мммм",
            "__v": 0
            },
        "weight": 120,
        "__v": 0
    }
]
```

### POST /dishes
Adds to Dish instance in a database.

|Param|Type|
|--|--|
|title|`string`|
|description|`string`|
|category|`string`|
|ingredients|`array`|
|additionalIngredients|`array`|
|price|`number`|
|weight|`number`|

Example request:
```json
{
    "ingredients": ["5dd09c2a0cdb777077a0bec8"],
    "additionalIngredients": ["5dd1b0a91c9d440000c0d815"],
    "price": 80,
    "title": "Сырники из творога",
    "description": "Bкусно",
    "category": "5dd19ed91c9d440000c0d802",
    "weight": 120
}
```
Example response:
```json
{
    "message": "Successfully created!"
}
```

### GET /dishes/:id
Returns a Dish instance by id.

Example response:
```json
{
    "ingredients": [
        {
            "price": 10,
            "_id": "5dd09c2a0cdb777077a0bec8",
            "title": "Творог",
            "restInStock": 10000,
            "description": "",
            "__v": 0
        }
    ],
    "additionalIngredients": [
        {
            "price": 40,
            "_id": "5dd1b0a91c9d440000c0d815",
            "title": "Шоколад",
            "restInStock": 450,
            "description": "Черный шоколад с высоким содержанием какао-масла",
            "__v": 0
        }
    ],
    "price": 80,
    "_id": "5dd0a0bd40d38b71a3adc7de",
    "title": "Сырники из творога",
    "description": "Bкусно",
    "category": {
        "_id": "5dd19ed91c9d440000c0d802",
        "title": "Десерты",
        "description": "мммм",
        "__v": 0
        },
    "weight": 120,
    "__v": 0
}
```
### PUT /dishes/:id
Updates to Dish instance in a database.

|Param|Type|
|--|--|
|title|`string`|
|description|`string`|
|category|`string`|
|ingredients|`array`|
|additionalIngredients|`array`|
|price|`number`|
|weight|`number`|

Example request:
```json
{
    "ingredients": ["5dd09c2a0cdb777077a0bec8"],
    "additionalIngredients": ["5dd1b0a91c9d440000c0d815"],
    "price": 80,
    "title": "Сырники из творога",
    "description": "Bкусно",
    "category": "5dd19ed91c9d440000c0d802",
    "weight": 120
}
```
Example response:
```json
{
    "message": "Successfully updated!"
}
```
### DELETE /dishes/:id
Delete a Dish instance by id.

Example response:
```json
{
    "message": "Successfully deleted!"
}
```
